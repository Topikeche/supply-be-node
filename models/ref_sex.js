'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ref_sex = sequelize.define('Ref_sex', {
    id: {
      type: DataTypes.INTEGER(4),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
    },
    sex: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
		tableName: 'ref_sex',
		timestamps: false
  });  
  return Ref_sex;
};