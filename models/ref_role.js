'use strict';
module.exports = (sequelize, DataTypes) => {
  const Ref_role = sequelize.define('Ref_role', {
    id: {
      type: DataTypes.INTEGER(4),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
    },
    role: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
		tableName: 'ref_role',
		timestamps: false
	});
  Ref_role.associate = function(models) {
    // associations can be defined here
  };
  return Ref_role;
};