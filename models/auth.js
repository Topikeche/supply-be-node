module.exports = (sequelize, DataTypes) => {
  const Auth = sequelize.define('Auth', {
    id: {
			type: DataTypes.INTEGER(4),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
		},
		email: {
			type: DataTypes.STRING(100),
			allowNull: false
		},
		password: {
			type: DataTypes.STRING(100),
			allowNull: false
		},
		role: {
			type: DataTypes.INTEGER(2),
			allowNull: false
		},
		reg_token: {
			type: DataTypes.TEXT,
			allowNull: false
		},
    id_employee :{
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: null
		}
  }, {
		tableName: 'auth',
		timestamps: false
  });
  
  Auth.associate = (models) => {
  };

  return Auth;
};