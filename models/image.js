'use strict'
module.exports = (sequelize, DataTypes) => {
	const Image = sequelize.define('Image', {
	  type: {
			type: DataTypes.INTEGER(4)
	  },
	  path: {
			type: DataTypes.STRING(30)
		},
		sub_path: {
			type: DataTypes.STRING(30)
		},
	  name: {
			type: DataTypes.STRING(150)
		},
	  original_name: {
			type: DataTypes.STRING(150)
		},
		mime: {
			type: DataTypes.STRING(15)
		}
	}, {
		tableName: 'image',
		timestamps: false
	});
  Image.associate = function(models) {
    // associations can be defined here
  };
	return Image;
};