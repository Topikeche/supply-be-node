module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    id: {
			type: DataTypes.INTEGER(4),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
		},
		full_name: {
			type: DataTypes.STRING(100),
			allowNull: false
		},
		address: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		birth_date: {
			type: DataTypes.DATE,
			allowNull: true
    },
    sex: {
      type: DataTypes.STRING(1),
      allowNull: true,
      references: {
        model: 'Ref_sex',
        key: 'id'
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING(14),
      allowNull: true
    },
    join_date: {
      type: DataTypes.DATE,
      allowNull: false
    }, 
    leave_date: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    },
    photo: {
      type: DataTypes.INTEGER(10),
      allowNull: true,
      defaultValue: null
    },
    identity_card: {
      type: DataTypes.INTEGER(10),
      allowNull: true,
      defaultValue: null
    },
    role :{
      type: DataTypes.STRING(1),
      allowNull: false,
      references: {
        model: 'Ref_role',
        key: 'id'
      }
    },
    verified: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    }
  }, {
		tableName: 'employee',
		timestamps: false
  });
  
  Employee.associate = (models) => {
    Employee.belongsTo(models.Ref_sex, {
      foreignKey: 'sex',
      as: 'the_sex'
    })
    Employee.belongsTo(models.Ref_role, {
      foreignKey: 'role',
      as: 'the_role'
    })
    Employee.belongsTo(models.Image, {
      foreignKey: 'photo',
      as: 'the_photo'
    })
    Employee.belongsTo(models.Image, {
      foreignKey: 'identity_card',
      as: 'the_identity_card'
    })
  };

  return Employee;
};