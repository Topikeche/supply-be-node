'use strict'
module.exports = (sequelize, DataTypes) => {
	const Ref_image = sequelize.define('Ref_image', {
		id: {
      type: DataTypes.INTEGER(4),
			autoIncrement: true,
			primaryKey: true,
			allowNull: false
    },
    type: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
		tableName: 'ref_image',
		timestamps: false
	})
  Ref_image.associate = function(models) {
    // associations can be defined here
	}
	return Ref_image
}