const multer = require('multer')
const path = require('path')

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, __basedir + '/assets/tmp/')
	},
	filename: (req, file, cb) => {
		// format name TIME-RANDOMSTRING-ORIGINALNAME
		let unique = [...Array(5)].map(a => Math.random().toString(36).substr(2)).join('')
		// console.log(Date.now() + '-1-' + req.body.type + path.extname(file.originalname))
		cb(null, (Date.now() + '101' + unique + path.extname(file.originalname)))
	}
})
const uploadImage = multer({ storage: storage })
 
module.exports = uploadImage;