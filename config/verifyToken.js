const jwt = require('jsonwebtoken')
const { resFailed } = require('../helpers/rensponse')

const secret = 'kdfjynbleoktovio346937mb34n3yi2yjslkajdgo83'

const verifyToken = () => {
	return function(req, res, next) {
		const bearerHeader = req.headers.authorization
		if(typeof bearerHeader !== 'undefined') {
			bearerToken = bearerHeader.replace("Bearer ",'')
			jwt.verify(bearerToken, secret, (err, payload) => {
				if(err) res.status(403).json(resFailed('Not Authorized', err))
				else next()
			})
		}
		else {
			res.status(403).json(resFailed('Not Authorized', 'Token Required'))
		}
	}
}

module.exports = verifyToken