const Ref_sex = require('../models').Ref_sex

exports.getAllSex = (req, res) => {
	Ref_sex.findAll({
		attributes: {}
	}).then(data => {
		if (data == null || data == '')
			res.status(401).json({
				"status": false,
				"message": "Data not found"
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}