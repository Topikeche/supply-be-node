const Auth 				= require('../models').Auth
const utils				= require('../helpers/utils')
const md5					= require('md5')
const nodemailer	= require('nodemailer')
const fs					= require('fs')
const handlebars	= require('handlebars');

const { resSuccess, resFailed }	= require('../helpers/rensponse')

const transporter	= nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'ananda.putri.valentina@gmail.com',
		pass: '13091992'
	}
})

exports.asle = (req, res) => {
	data = {
		userId: 1,
		email: 'dana.sejahtera@mail.com',
		role: 1
	}
	token = utils.generateToken(data)
	res.json({'message': 'we did it...', token })
}

exports.signIn = async (req, res) => {
	try {		
		const user = await Auth.findOne(
			{ attributes: { exclude: ['password'] } },
			{ where: { email: req.body.email, password: md5(req.body.password) } }
		)
		if(user == null) {
			res.status(403).json(resFailed('Incorrect username & password', null))
		} else {
			token = utils.generateToken(user)
			res.status(200).json(resSuccess('Authenticated', token))
		}
	} catch (err) {
		res.status(403).json(resFailed('Incorrect username & password', err))
	}
}

exports.signUp = async (req, res) => {
	try {
		let regToken = await utils.generateToken({ email: req.body.email, role: req.body.email })
		await Auth.create({
			email: req.body.email,
			password: md5(req.body.password),
			role: req.body.role,
			reg_token: regToken
		})
		let template = await handlebars.compile(fs.readFileSync(__basedir + '/assets/templates/emailVerification.html', {encoding: 'utf-8'}))
		let replacement = {
			username: req.body.fullname,
			link: 'http://localhost:3003/api/auth/confirm/' + regToken
		}
		let mailOpt = {
			from: '"[No-Reply] SuAPP Support" <ananda.putri.valentina@gmail.com>',
			to: req.body.email,
			subject: 'SuAPP E-mail Confirmation',
			html: template(replacement)
		}

		transporter.sendMail(mailOpt, (err, info) => {
			if(err) res.json(resFailed('Registration Failed', err))
			else res.json(resSuccess('Registration Succes!', info))
		})
	} catch (error) {
		res.status(400).json(resFailed('Registration Failed! root', error))
	}
}

exports.createCredential = async (req, res) => {
	
}