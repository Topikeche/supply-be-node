const Ref_role = require('../models').Ref_role

exports.getAllRole = (req, res) => {
	Ref_role.findAll({
		attributes: {}
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found"
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.getOneRole = (req, res) => {
	Ref_role.findOne({
		attributes: {},
		where: { id: req.params.idRole }
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.addOneRole = (req, res) => {
	Ref_role.create({
		role: req.body.role
	}).then(data => {
		res.json({
			"status": true,
			"message": "Request success",
			"data": data
		})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.updateOneRole = (req, res) => {
	Ref_role.findOne({ 
		where: { id: req.params.idRole }
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			data.updateAttributes({
				role: req.body.role
			}).then(data2 => {
				if(data2 == null || data2 == ''){
					res.status(404).json({
						"status": false,
						"message": "Data not Found",
						"data": data
					})
				} else{
					res.status(200).json({
						"status": true,
						"message": "Request success",
						"data": data2
					})
				}
			}).catch(err => {
				res.status(400).send(err)
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.deleteOneRole = (req, res) => {
	Ref_role.destroy({
		where: { id: req.params.idRole }
	}).then(data => {
		if(data == null || data == ''){
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		} else{
			res.status(200).json({
				"status": true,
				"message": "Request success",
				"data": data
			})
		}
	}).catch(err => {
		res.status(400).send(err)
	})
}