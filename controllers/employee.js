const Employee = require('../models').Employee
const Image = require('../models').Image
const Ref_sex = require('../models').Ref_sex
const Ref_role = require('../models').Ref_role

exports.getAllEmployee = (req, res) => {
	Employee.findAll({
		attributes: {},
		include: [
			{
				model: Ref_sex,
				as: 'the_sex'
			},
			{
				model: Ref_role,
				as: 'the_role'
			},
			{
				model: Image,
				as: 'the_photo',
				attributes: { exclude: ['original_name', 'mime'] }
			}
		]
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.getOneEmployee = (req, res) => {
	Employee.findOne({
		attributes: {},
		include: [
			{
				model: Ref_sex,
				as: 'the_sex'
			},
			{
				model: Ref_role,
				as: 'the_role'
			},
			{
				model: Image,
				as: 'the_photo',
			},
			{
				model: Image,
				as: 'the_identity_card'
			}
		],
		where: { id: req.params.idEmployee }
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.addOneEmployee = (req, res) => {
	Employee.create({
		full_name: req.body.full_name,
		address: req.body.address,
		birth_date: req.body.birth_date,
		sex: req.body.sex,
		join_date: req.body.join_date,
		role: req.body.role
	}).then(data => {
		res.json({
			"status": true,
			"message": "Request success",
			"data": data
		})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.updateOneEmployee = (req, res) => {
	Employee.findOne({
		where: { id: req.params.idEmployee }
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			data.updateAttributes({
				full_name: req.body.full_name,
				address: req.body.address,
				birth_date: req.body.birth_date,
				sex: req.body.sex,
				join_date: req.body.join_date,
				leave_date: req.body.leave_date,
				role: req.body.role
			}).then(data2 => {
				if (data2 == null || data2 == '')
					res.status(404).json({
						"status": false,
						"message": "Data not found",
						"data": data2
					})
				else
					res.json({
						"status": true,
						"message": "Request success",
						"data": data2
					})
			}).catch(err2 => {
				res.status(400).send(err2)
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.deleteOneEmployee = (req, res) => {
	Employee.destroy({
		where: { id: req.params.idEmployee }
	}).then(data => {
		if (data == null || data == '' || data == 0)
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}