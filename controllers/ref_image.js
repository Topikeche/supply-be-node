const Ref_image = require('../models').Ref_image

exports.getAllImageType = (req, res) => {
	Ref_image.findAll({
		attributes: {}
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found"
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.getOneImageType = (req, res) => {
	Ref_image.findOne({
		attributes: {},
		where: { id: req.params.idType }
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			res.json({
				"status": true,
				"message": "Request success",
				"data": data
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.addOneImageType = (req, res) => {
	Ref_image.create({
		type: req.body.type
	}).then(data => {
		res.json({
			"status": true,
			"message": "Request success",
			"data": data
		})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.updateOneImageType = (req, res) => {
	Ref_image.findOne({ 
		where: { id: req.params.idType }
	}).then(data => {
		if (data == null || data == '')
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		else
			data.updateAttributes({
				type: req.body.type
			}).then(data2 => {
				if(data2 == null || data2 == ''){
					res.status(404).json({
						"status": false,
						"message": "Data not Found",
						"data": data
					})
				} else{
					res.status(200).json({
						"status": true,
						"message": "Request success",
						"data": data2
					})
				}
			}).catch(err => {
				res.status(400).send(err)
			})
	}).catch(err => {
		res.status(400).send(err)
	})
}

exports.deleteOneImageType = (req, res) => {
	Ref_image.destroy({
		where: { id: req.params.idType }
	}).then(data => {
		if(data == null || data == ''){
			res.status(404).json({
				"status": false,
				"message": "Data not found",
				"data": data
			})
		} else{
			res.status(200).json({
				"status": true,
				"message": "Request success",
				"data": data
			})
		}
	}).catch(err => {
		res.status(400).send(err)
	})
}