const sharp = require('sharp')
const fs = require('fs')
const { generateDateDir, directoryExist, createDateDir, removeImage } = require('../helpers/utils')
const { resSuccess, resFailed } = require('../helpers/rensponse')

const connection = require('../models')
const Employee = require('../models').Employee
const Image = require('../models').Image

exports.addOneImage = async (req, res) => {
	let parentPath
	let subPath

	try {
		// let imageData = await fs.readFileSync(__basedir + '/assets/tmp/' + req.file.filename)
		// await sharp(imageData).resize(300).toBuffer()
		// await fs.writeFileSync(__basedir + '/assets/upload/public/' + '300-' + req.file.filename, imageData)

		
		if(req.body.imageType == '2') parentPath = '/assets/upload/private'
		else parentPath = '/assets/upload/public'
		subPath = generateDateDir()

		if(!directoryExist(__basedir + parentPath + subPath)) await createDateDir(__basedir + parentPath + subPath)

		let sharpImage = await sharp(__basedir + '/assets/tmp/' + req.file.filename).toBuffer()
		await sharp(sharpImage).toFile(__basedir + parentPath + subPath + req.file.filename)
		await sharp(sharpImage).resize(500).toFile(__basedir + parentPath + subPath + '500-' + req.file.filename)
		// JIKA PHOTOPROFILE(IMAGETYPE: 1) MAKA DIBUATKAN LAGI 1 FILE UKURAN KECIL
		if(req.body.imageType == '1') await sharp(sharpImage).resize(150).toFile(__basedir + parentPath + subPath + '150-' + req.file.filename)
		await fs.unlinkSync(__basedir + '/assets/tmp/' + req.file.filename)	

		let t = await connection.sequelize.transaction()
		let saveImage = await Image.create({
			type: req.body.imageType,
			path: parentPath,
			sub_path: subPath,
			name: req.file.filename,
			original_name: req.file.originalname,
			mime: req.file.mimetype
		}, { transaction: t })
		switch (req.body.imageType) {
			case '1':
				// await Employee.findOne({
				// 	where: { id: req.body.idUser }
				// }).then(data => {
				// 	data.updateAttributes({ photo: saveImage.id })
				// })
				await Employee.update(
					{ photo: saveImage.id },
					{ where: { id: req.body.idUser }, transaction: t }
				)
				break
			case '2':
				await Employee.update(
					{ identity_card: saveImage.id },
					{ where: { id: req.body.idUser }, transaction: t }
				)
				break
			default:
				break
		}
		
		await t.commit()
		res.json(resSuccess('Request Success', saveImage))		
	} catch(err){
		res.json(resFailed('Request Failed', err))
	}

	// try {
	// 	fs.writeFileSync(__basedir + '/assets/upload/public/' + req.file.filename, imageData)
	// 	sharp(imageData).resize(300).toBuffer()
	// 		.then(data => {
	// 			fs.writeFileSync(__basedir + '/assets/upload/public/' + '300-' + req.file.filename, data)
	// 			fs.unlinkSync(__basedir + '/assets/tmp/' + req.file.filename)	
	// 		})
		
	// } catch (err) {
		
	// }

	// let imageData = fs.readFileSync(__basedir + '/assets/tmp/' + req.file.filename)
	// Image.create({
	// 	type: 1,
	// 	path:	'/assets/upload/public/',
	// 	name: req.file.originalname,
	// 	mime: req.file.mimetype
	// })
	// .then(img => {
	// 	try {
	// 		fs.writeFileSync(__basedir + '/assets/upload/public/' + req.file.filename, imageData);
	// 		sharp(imageData).resize(300).toBuffer().then(data => {
	// 			fs.writeFileSync(__basedir + '/assets/upload/public/' + '300-' + req.file.filename, data)
	// 		})
	// 		.catch(err => {
	// 			console.log(err)
	// 		})	
	// 		fs.unlinkSync(__basedir + '/assets/tmp/' + req.file.filename)	
	// 		res.json({'msg': 'File uploaded successfully!', 'file': req.file});
	// 	} catch (err) {
	// 		console.log(err);
	// 		res.json({'error': err});
	// 	}
	// })
}

exports.getOneImage64 = async (req, res) => {
	try {
		let row = await Image.findOne({ where: { id: req.params.idImage } })
		let image = new Buffer(fs.readFileSync(__basedir + row.path + row.sub_path + row.name), 'base64')
		res.writeHead(200, {
			'Content-Type': row.mime,
			'Content-Length': image.length
		})
		console.log(image)
		res.end(image)
		// res.json(resSuccess('Request Succes', image))
	} catch (err) {
		res.json(resFailed('Request Failde', err))
	}
}

exports.updateOneImage = async (req, res) => {
	let parentPath
	let subPath
	try {
		if(req.body.imageType == '2') parentPath = '/assets/upload/private'
		else parentPath = '/assets/upload/public'
		subPath = generateDateDir()
		if(!directoryExist(__basedir + parentPath + subPath)) await createDateDir(__basedir + parentPath + subPath)

		let sharpImage = await sharp(__basedir + '/assets/tmp/' + req.file.filename).toBuffer()
		sharp(sharpImage).toFile(__basedir + parentPath + subPath + req.file.filename)
		sharp(sharpImage).resize(500).toFile(__basedir + parentPath + subPath + '500-' + req.file.filename)
		// JIKA PHOTOPROFILE(IMAGETYPE: 1) MAKA DIBUATKAN LAGI 1 FILE UKURAN KECIL
		if(req.body.imageType == '1') sharp(sharpImage).resize(150).toFile(__basedir + parentPath + subPath + '150-' + req.file.filename)
		fs.unlinkSync(__basedir + '/assets/tmp/' + req.file.filename)	

		let oldImage = await Image.findOne({ where: { id: req.params.idImage } })
		removeImage(__basedir + oldImage.get('path') + oldImage.get('sub_path'), oldImage.get('name'), req.body.imageType)

		let newImage = await Image.update(
			{ 
				path: parentPath,
				sub_path: subPath,
				name: req.file.filename,
				original_name: req.file.originalname,
				mime: req.file.mimetype
			},
			{ where: { id: req.params.idImage } }
		)
		res.json(resSuccess('Request Success', newImage))	
	} catch (err) {
		res.json(resFailed('Request Failed', err))
	}
}

exports.deleteOneImage = async (req, res) => {
	try {
		oldImage = await Image.findOne({ where: { id: req.params.idImage } })
		await Image.destroy({ where: {id: req.params.idImage } })
		await removeImage(__basedir + oldImage.get('path') + oldImage.get('sub_path'), oldImage.get('name'), oldImage.get('type'))
		res.json(resSuccess('Request Success', oldImage))
	} catch (err) {
		res.json(resFailed('Request Failed', err))
	}
}