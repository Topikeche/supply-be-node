exports.resSuccess = (message, data) => {
	return {
		status: true,
		message: message,
		data: data
	}
}
exports.resFailed = (message, data) => {
	return {
		status: false,
		message: message,
		data: data
	}
} 