const fs		= require('fs')
const fse 	= require('fs-extra') 
const jwt		= require('jsonwebtoken')

const secret = 'kdfjynbleoktovio346937mb34n3yi2yjslkajdgo83'

exports.generateDateDir = () => {
	date = new Date()
	return '/' + date.getFullYear() + '/' + (date.getMonth()+1) + '/' + date.getDate() + '/'
}

exports.createDateDir = (path) => {
	fse.ensureDir(path, err => {
		console.log(err)
	})
}

exports.directoryExist = (path) => {
	if(fs.existsSync(path)) return true
	else return false
}

exports.removeImage = (path, name, imageType) => {
	fs.unlinkSync(path + name)
	fs.unlinkSync(path + '500-' + name)
	if(imageType == '1') fs.unlinkSync(path + '150-' + name)
}

exports.generateToken = (data) => {
	token = jwt.sign({user: data}, secret, { expiresIn: '604800s'})
	return token
}

exports.verifyToken = () => {
	return 'masuk'
}