// controller here
const Employee 		= require('../controllers/employee')
const Auth 				= require('../controllers/auth')
const Ref_sex 		= require('../controllers/ref_sex')
const Ref_role 		= require('../controllers/ref_role')
const Ref_image 	= require('../controllers/ref_image')


const Image 			= require('../controllers/image')
const upload 			= require('../config/upload')
const verifyToken	= require('../config/verifyToken')

module.exports = (app) => {
	app.route('/api')
			.get((req, res) => res.status(200).json({ message: 'Welcome to our supply API'}))
	

	app.route('/api/employee')
			.get(Employee.getAllEmployee)
			.post(Employee.addOneEmployee)
	app.route('/api/employee/:idEmployee')
			.get(Employee.getOneEmployee)
			.put(Employee.updateOneEmployee)
			.delete(Employee.deleteOneEmployee)

	app.route('/api/role')
			.get(Ref_role.getAllRole)
			.post(Ref_role.addOneRole)
	app.route('/api/role/:idRole')
			.get(Ref_role.getOneRole)
			.put(Ref_role.updateOneRole)
			.delete(Ref_role.deleteOneRole)
	
	app.route('/api/image/type')
			.get(Ref_image.getAllImageType)
			.post(Ref_image.addOneImageType)
	app.route('/api/image/type/:idType')
			.get(Ref_image.getOneImageType)
			.put(Ref_image.updateOneImageType)
			.delete(Ref_image.deleteOneImageType)

	app.route('/api/image/file')
			.post(upload.single('imageUpload'), Image.addOneImage)
	app.route('/api/image/file/:idImage')
			.get(Image.getOneImage64)
			.put(upload.single('imageUpload'), Image.updateOneImage)
			.delete(Image.deleteOneImage)

	app.route('/api/sex')
			.get(Ref_sex.getAllSex)


	app.route('/api/auth/signin')
			.get(verifyToken(), Auth.asle)
			.post(Auth.signIn)
	app.route('/api/auth/signup')
			.post(Auth.signUp)
}