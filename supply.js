global.__basedir = __dirname;

const express= require('express')
const app = express()
const routes = require('./routes/routes')
const morgan = require('morgan')
const bodyParser = require('body-parser')

// var storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, 'assets/upload/');
//   },
//   filename: (req, file, cb) => {
//     cb(null, Date.now() + '-' + file.originalname);
//   }
// });
// const upload = multer({ storage: storage }); 
// const fs = require('fs')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))
app.use(morgan('dev'))
app.use('/api/static/', express.static('assets/upload/public'))

app.get('/', (req, res) => {
	console.log('printing')
	res.send('hello from the other sideee....')
})
// app.post('/upload', upload.single('file-to-upload'), (req, res) => {
// 	console.log('the name: ', req.file.originalname, req.file.mimetype, req.file.path)
//   res.send('Upload OK');
// });


// const Upload = require('./config/upload')
// const Image = require('./controllers/image')
// app.post('/api/upload/image', Upload.single('fileUpload'), Image.addImage)

app.listen(3003, () => {
	routes(app)
	console.log('your server is up on 3003...')
})

module.exports = app